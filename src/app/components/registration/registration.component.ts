declare var sub: any

//TODO to remove declare var any
//npm install @types/node --save-dev

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  profileForm: FormGroup;
  submitted = false;
  date =(formatDate(new Date(), 'yyyy', 'en'));
  // age = new FormControl('');
  // currentAge = <FormControl>this.age;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.profileForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.maxLength(14), Validators.pattern("^[a-z, A-Z]*$")]],
      lastName: ['', [Validators.required, Validators.maxLength(20), Validators.pattern("^[0-9, a-z, A-Z]*$")]],
      age: ['', [Validators.required, Validators.min(1),  Validators.max(200), Validators.maxLength(3), Validators.pattern("^[1-9]+[0-9]*$")]],

    })

    this.profileForm.valueChanges.subscribe(console.log)

  }

get age() {
  return this.profileForm.get('age')
}
get firstName() {
  return this.profileForm.get('firstName')
}
get lastName() {
  return this.profileForm.get('lastName')
}

getErrorMessageFirst() {
  return this.firstName.hasError('required') ? 'You must enter a value' :
          this.firstName.hasError('maxlength') ? 'Too long' :
          this.firstName.hasError('pattern') ? 'Only letters' : '' ;

}
getErrorMessageLast() {
  return this.lastName.hasError('required') ? 'You must enter a value' :
          this.lastName.hasError('maxlength') ? 'Too long' :
          this.lastName.hasError('pattern') ? 'No specialcharacters' : '' ;

}

getErrorMessageAge() {
  return this.age.hasError('required') ? 'You must enter a value' :
          this.age.hasError('max') ? 'Zombie ??!!🧟‍♂️  🤯' :
          this.age.hasError('min') ? '0 is not valid age' : '';
}


onSubmit() {
console.log(this.profileForm.errors);

let d = this.date
let yearTxt: string= JSON.stringify(d);
let year = parseInt(yearTxt.split('').slice(1,5).join(''))
let dateBirth = (year- this.profileForm.value.age)

    this.submitted = true;

    // stop here if form is invalid
    if (this.profileForm.invalid) {
        return;
    }


  // build tool to remove --->>>
  // console.warn(JSON.stringify(this.profileForm.value.firstName + this.profileForm.value.lastName + dateBirth));

let dataName = JSON.stringify((`First name: ${this.profileForm.value.firstName}`));
let dataSurname = JSON.stringify((`Last name: ${this.profileForm.value.lastName}`));
var dataDate = JSON.stringify((`Year of birth: ${this.profileForm.value.dateBirth}`));
let myContainer = <HTMLElement> document.querySelector(".info");

//checkout type of dataDate - not display correctly undefined TODO
//change inner htmlto inner txt TODO
myContainer.innerHTML = `<p> ${dataName}</p> <br /> <p> ${dataSurname}</p> <br /> <p> ${dataDate}</p> <br />`;


  alert(JSON.stringify((`First name: ${this.profileForm.value.firstName}\
                         Last name: ${this.profileForm.value.lastName}\
                         Year of birth: ${dateBirth}`)));
}}
